# ESP32 vector scope driver #
Drives an osciloscope in XY mode to draw vector graphics sent over the USB port.
* Uses the two internal 8 bit DAC's in i2s mode.
* Includes python library to communicate with the driver


