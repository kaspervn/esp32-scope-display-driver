#include <stdio.h>
#include <math.h>
#include <vector>
#include <cassert>
#include <cstdlib>

#ifndef SIM

    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"

    #include "driver/i2s.h"
    #include "driver/uart.h"
    #include "driver/gpio.h"
    #include "driver/dac.h"
    #include "esp_system.h"

    #define I2S_NUM         (I2S_NUM_0)
    #define UART_NUM        (UART_NUM_0)

#else

    #include "FreeRTOS.h"
    #include "task.h"
    
    #include "main_sim.h"

#endif


#define GFX_BUFFERS_SIZE (1024) // in number of commands
#define GEN_SAMPLE_RATE (200000)


typedef struct {
    uint8_t x, y;
} __attribute__((__packed__)) point_t;

typedef enum {
    GFX_CMD_GOTO = 0, //sets current point
    GFX_CMD_LINE_TO, //draws line to point
    GFX_CMD_SET_BRIGHTNESS,
    GFX_CMD_SWAP, //swap display buffers
} gfx_cmd_type_t;

typedef struct {
    gfx_cmd_type_t type:8;
    union {
        point_t point;
        uint8_t brightness;
    };

} __attribute__((__packed__)) gfx_cmd_t;

typedef struct {
    std::vector<gfx_cmd_t> **next_buffer;
    
} gen_waveform_task_parameters_t;

size_t gen_line_low(int32_t* buf, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1)
{
    int16_t dx = x1 - x0;
    int16_t dy = y1 - y0;
    int16_t yi = 1;
    if(dy < 0)
    {
        yi = -1;
        dy = -dy;
    }
    int16_t D = (2 * dy) - dx;
    int16_t y = y0;
    
    for(int16_t x = x0; x < x1; x++)
    {
        *buf = ((y << 8) & 0xFFFF) | ((x << 24) & 0xFFFF0000);
        buf += 1;
        if(D > 0)
        {
            y = y + yi;
            D += (2 * (dy - dx));
        } else {
            D += 2 * dy;
        }
    }
    
    return x1 - x0;
}

size_t gen_line_high(int32_t* buf, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1)
{
    int16_t dx = x1 - x0;
    int16_t dy = y1 - y0;
    int16_t xi = 1;
    if(dx < 0)
    {
        xi = -1;
        dx = -dx;
    }
    int16_t D = (2 * dx) - dy;
    int16_t x = x0;
    
    for(int16_t y = y0; y < y1; y++)
    {
        *buf = ((y << 8) & 0xFFFF) | ((x << 24) & 0xFFFF0000);
        buf += 1;
        if(D > 0)
        {
            x = x + xi;
            D += (2 * (dx - dy));
        } else {
            D += 2 * dx;
        }
    }
    
    return y1 - y0;
}

size_t gen_line(int32_t* buf, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1)
{
    if(abs(y1 - y0) < abs(x1 - x0)) {
        if(x0 > x1)
            return gen_line_low(buf, x1, y1, x0, y0);
        else
            return gen_line_low(buf, x0, y0, x1, y1);
    } else {
        if(y0 > y1)
            return gen_line_high(buf, x1, y1, x0, y0);
        else
            return gen_line_high(buf, x0, y0, x1, y1);
    }
}

void synth_line(
        point_t start,
        point_t end,
        uint16_t brightness)
{
    static int32_t buf[512];

    size_t buf_len = gen_line(buf, start.x, start.y, end.x, end.y);

    size_t i2s_bytes_write = 0;
#ifndef SIM
    i2s_write(I2S_NUM, buf, buf_len*4, &i2s_bytes_write, portMAX_DELAY);
#else
    i2s_write_sim(buf, buf_len*4, &i2s_bytes_write);
#endif
    assert(i2s_bytes_write == buf_len*4);
    
}

// old and old function to generate lines.
/*void synth_line2(
        point_t start,
        point_t end,
        uint16_t brightness)
{
    int16_t dx = end.x - start.x;
    int16_t dy = end.y - start.y;

    float length = fabs(dx) + fabs(dy);

    size_t duration = (size_t)(GEN_SAMPLE_RATE * length * (1.0 / 100000.0));

    int32_t buf[duration];

    for(int n = 0; n < duration; n++)
    {
        uint8_t x = (uint8_t)((float)start.x + (float)dx * n / duration);
        uint8_t y = (uint8_t)((float)start.y + (float)dy * n / duration);
        
        buf[n] = ((y << 8) & 0xFFFF) | ((x << 24) & 0xFFFF0000);
    }

    size_t i2s_bytes_write = 0;
    i2s_write(I2S_NUM, buf, duration*4, &i2s_bytes_write, portMAX_DELAY);
    assert(i2s_bytes_write == duration*4);
    
}*/

void gen_waveform_task(void* _params)
{
    gen_waveform_task_parameters_t* params = (gen_waveform_task_parameters_t*)_params;
  
    std::vector<gfx_cmd_t>* cmd_buf = *params->next_buffer;

    for(;;)
    {
        
        if(xTaskNotifyWait(0, 0, NULL, 0))
        {
            cmd_buf = *params->next_buffer;
        }
        
        point_t prev_p = {.x = 0, .y = 0};
        for(size_t n = 0; n < cmd_buf->size(); n++)
        {
            gfx_cmd_t cmd = cmd_buf->at(n);
            switch(cmd.type)
            {
                case GFX_CMD_GOTO:
                    prev_p = cmd.point;
                    break;
                case GFX_CMD_LINE_TO:
                    synth_line(prev_p, cmd.point, 255);
                    prev_p = cmd.point;
                    break;
                default:
                    break;
            }
        }
    }

}

static size_t read_bytes_from_cmd_input(void* output, size_t num_of_bytes, int timeout_ticks) {
#ifndef SIM
    return uart_read_bytes(UART_NUM, output, num_of_bytes, timeout_ticks);
#else
    return cmd_intput_sim(output, num_of_bytes, timeout_ticks);
#endif
}

typedef struct {
    std::vector<gfx_cmd_t> *gfx_cmd_buffers;
    std::vector<gfx_cmd_t>** next_buffer;
    TaskHandle_t *gen_waveform_task_handle;
    
} receive_commands_task_parameters_t;

void receive_commands_task(void* _params)
{
    size_t display_buffer = 0;
    size_t draw_buffer = 1;
    std::vector<gfx_cmd_t> *gfx_cmd_buffers = ((receive_commands_task_parameters_t*)_params)->gfx_cmd_buffers;
    std::vector<gfx_cmd_t>** next_buffer = ((receive_commands_task_parameters_t*)_params)->next_buffer;
    TaskHandle_t *gen_waveform_task_handle = ((receive_commands_task_parameters_t*)_params)->gen_waveform_task_handle;
    
    for(;;)
    {
        gfx_cmd_t cmd;
        if(read_bytes_from_cmd_input(&cmd, sizeof(cmd), 5000 / portTICK_PERIOD_MS) == sizeof(cmd))
        {
            if(cmd.type == GFX_CMD_SWAP)
            {
                display_buffer = draw_buffer;
                draw_buffer = (draw_buffer + 1) % 3;
                *next_buffer = &((gfx_cmd_buffers)[display_buffer]);
                xTaskNotify(*gen_waveform_task_handle, 0, eNoAction);
                
                (gfx_cmd_buffers)[draw_buffer].clear();
            } else {
                (gfx_cmd_buffers)[draw_buffer].push_back(cmd);
            }
        } else {
            //timeout
        }
        
    }
}

void init_i2s(void)
{
#ifndef SIM
    i2s_config_t i2s_config = {
        .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
        .sample_rate = GEN_SAMPLE_RATE,
        .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
        .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
        .communication_format = (i2s_comm_format_t)0,
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
        .dma_buf_count = 8,
        .dma_buf_len = 64,
        .use_apll = false,
        .tx_desc_auto_clear = false,
        .fixed_mclk = 0
    };

    i2s_driver_install(I2S_NUM, &i2s_config, 0, NULL);
    i2s_set_clk(I2S_NUM, GEN_SAMPLE_RATE, I2S_BITS_PER_SAMPLE_16BIT, I2S_CHANNEL_STEREO);
    i2s_set_pin(I2S_NUM, NULL);
#endif
}

void init_uart(void)
{
#ifndef SIM
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 0,
        .source_clk = UART_SCLK_APB,
        
    };
    ESP_ERROR_CHECK(uart_driver_install(UART_NUM, 1024 * 2, 0, 0, NULL, 0));
    ESP_ERROR_CHECK(uart_param_config(UART_NUM, &uart_config));
#endif
}


extern "C" void app_main(void)
{
    init_i2s();
    init_uart();

    std::vector<gfx_cmd_t> gfx_cmd_buffers[3] = {
        std::vector<gfx_cmd_t>(),
        std::vector<gfx_cmd_t>(),
        std::vector<gfx_cmd_t>()};
        
    for(int n = 0; n < 3; n++)
     gfx_cmd_buffers[n].reserve(GFX_BUFFERS_SIZE);
    
    std::vector<gfx_cmd_t>* next_buffer = &gfx_cmd_buffers[0];
    gen_waveform_task_parameters_t gen_task_params = {
        .next_buffer = &next_buffer
    };
    
    
    TaskHandle_t gen_waveform_task_handle = NULL;
    xTaskCreate(gen_waveform_task, "gen_waveform_task", 1024*20, (void*)&gen_task_params, tskIDLE_PRIORITY, &gen_waveform_task_handle);
    configASSERT(gen_waveform_task_handle);
    
    receive_commands_task_parameters_t receive_commands_task_params = {
        .gfx_cmd_buffers = gfx_cmd_buffers,
        .next_buffer = &next_buffer,
        .gen_waveform_task_handle = &gen_waveform_task_handle
    };
    
    TaskHandle_t receive_commands_task_handle = NULL;
    xTaskCreate(receive_commands_task, "receive_commands", 1024*20, (void*)&receive_commands_task_params, tskIDLE_PRIORITY, &receive_commands_task_handle);
    configASSERT(receive_commands_task_handle);
    
#ifdef SIM
    vTaskStartScheduler();
#else 
    
    for(;;){}
#endif
}
