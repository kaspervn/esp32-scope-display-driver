#!/usr/bin/python
import socket
from itertools import chain

import scopedriver
import time
import datetime
import random
from numpy import array, linspace
import math
from dataclasses import dataclass

@dataclass
class Flake:
    id: int
    pos: array
    angle: float
    rot_speed: float
    num_lines: int

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

def new_flake():
    return Flake(random.randint(0, 2**32), array([random.uniform(-10, 10), 1.0, random.uniform(0.3, 1.0)]), 0, random.uniform(-1.0, 1.0), random.randint(3, 3))

def initial_flake():
    f = Flake(random.randint(0, 2**32), array([random.uniform(-10, 10), 0.1 + random.random(), random.uniform(0.3, 1.0)]), 0, random.uniform(-1.0, 1.0), random.randint(3, 3))
    return f

def flake_paths(f: Flake):
    da = f.angle
    p = f.pos
    r = 0.025 / f.pos[2]

    return [[(p[0] + r*math.cos(da + a), p[1] + r*math.sin(da + a)),
             (p[0] - r*math.cos(da + a), p[1] - r*math.sin(da + a))] for a in linspace(0, 2*math.pi, num=f.num_lines + 1)]



# serial = serial.Serial('/dev/ttyUSB6', baudrate=115200)
flakes = set(initial_flake() for _ in range(120))

wind_sinus_frequencies = [random.uniform(0.2, 0.4) for _ in range(10)]


driver = scopedriver.ScopeDriver("sim")

while True:

    t = time.time()
    wind_speed = sum(math.sin(t * f) for f in wind_sinus_frequencies)

    removed_flakes = []
    new_flakes = []
    for f in flakes:
        f.pos = f.pos + array([wind_speed * 0.005 / f.pos[2], -0.005 / f.pos[2], 0])
        f.angle += 0.03 * f.rot_speed
        if f.pos[1] < 0:
            removed_flakes.append(f)
            new_flakes.append(new_flake())

    for flake in removed_flakes:
        flakes.remove(flake)


    for flake in new_flakes:
        flakes.add(flake)

    now = datetime.datetime.now()

    driver.send_paths(list(chain.from_iterable(map(flake_paths, filter(lambda f: 0 < f.pos[0] < 1, flakes)))))

    time.sleep(.03)

driver.close_connection()
