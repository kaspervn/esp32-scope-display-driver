#!/usr/bin/python
import scopedriver
import time
import datetime

font_base_points = [(0., 2.), (1., 2.), (0., 1.), (1., 1.), (0., 0.), (1., 0.)]

font_numbers_paths_indices = [
	[(0,1,5,4,0)], #0
	[(1,5)], #1
	[(0,1,3,2,4,5)], #2
	[(0,1,5,4),(3,2)], #3
	[(1,5),(3,2,0)], #4
	[(1,0,2,3,5,4)], #5
	[(1,0,4,5,3,2)], #6
	[(0,1,5)], #7
	[(0,1,5,4,0), (2,3)], #8
	[(5,1,0,2,3)]] #9
	
font_numbers = [[[font_base_points[idx] for idx in s] for s in paths] for paths in font_numbers_paths_indices]


def xpaths(paths, f):
	return [[f(p) for p in s] for s in paths]


def path_of_clock(a, b, c, d):

	font_scale = 0.09

	paths = xpaths(font_numbers[a], lambda p: (0.025 + font_scale*p[0], 0.5+font_scale*p[1])) + \
			xpaths(font_numbers[b], lambda p: (0.025 + .25 + font_scale*p[0], 0.5+font_scale*p[1])) + \
			xpaths(font_numbers[c], lambda p: (0.025 + 2*.25+.08 + font_scale*p[0], 0.5+font_scale*p[1])) + \
			xpaths(font_numbers[d], lambda p: (0.025 + 3*.25+.08 + font_scale*p[0], 0.5+font_scale*p[1]))

	return paths


driver = scopedriver.ScopeDriver("sim")

while(True):
	now = datetime.datetime.now()

	paths = path_of_clock(int(now.hour / 10), now.hour % 10, int(now.minute / 10), now.minute % 10)
	driver.send_paths(paths)

	time.sleep(.4)

driver.close_connection()