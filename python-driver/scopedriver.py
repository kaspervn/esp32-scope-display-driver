#!/usr/bin/python
import struct

import serial
import socket

GFX_CMD_GOTO = 0
GFX_CMD_LINE_TO = 1
GFX_CMD_SET_BRIGHTNESS = 2
GFX_CMD_SWAP = 3

def packet_cmd(t, a, b):
    return struct.pack('<BBB', t, a, b)


def packet_cmd_p(t, p):
    x = max(min(p[0], 1.0), 0.0)
    y = max(min(p[1], 1.0), 0.0)
    return packet_cmd(t, int(x*(2**8-1)), int(y*(2**8-1)))


def packet_goto(p):
    return packet_cmd_p(GFX_CMD_GOTO, p)


def packet_line_to(p):
    return packet_cmd_p(GFX_CMD_LINE_TO, p)


def packet_swap():
    return packet_cmd(GFX_CMD_SWAP, 0, 0)


def packets_drawlist(paths):
    for path in paths:
        yield packet_goto(path[0])
        yield from map(packet_line_to, path[1:])
    yield packet_swap()


def display_paths_serial(serial: serial.Serial, paths):
    for pkt in packets_drawlist(paths):
        serial.write(pkt)

    serial.flush()

def display_paths_socket(so: socket.socket, host, port, paths):
    for pkt in packets_drawlist(paths):
        so.sendto(pkt, (host, port))

class ScopeDriver:

    def __init__(self, connection: str):
        if connection == 'sim':
            so = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.send_frame_func = lambda *args, **kwargs: display_paths_socket(so, 'localhost', 8888, *args, **kwargs)
            self.close_func = lambda: so.close()
        else:
            s = serial.Serial(connection, baudrate=115200)
            self.send_frame_func = lambda *args, **kwargs: display_paths_serial(s, *args, **kwargs)
            self.close_func = lambda: s.close()

    def send_paths(self, paths):
        self.send_frame_func(paths)

    def close_connection(self):
        self.close_func()
