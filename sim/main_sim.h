#ifndef MAIN_SIM_H
#define MAIN_SIM_H

#ifdef __cplusplus
extern "C" {
#endif
    int i2s_write_sim(const void *src, size_t size, size_t *bytes_written);
    size_t cmd_intput_sim(void* output, size_t num_of_bytes, int timeout_ticks);
#ifdef __cplusplus
}
#endif


#endif
